/**
 * Defines base URL and template routing using UI Router.
 */
define(function(require) {
  var app = require('app');

  return app.module.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/");
    $stateProvider
        .state('/', {
          url: '/',
          templateUrl: 'app/home/home.html'
        })
        .state('test', {
          url: '/test',
          templateUrl: 'app/test/test.html'
        })
  })
});