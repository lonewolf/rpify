define(function(require) {
  var angular = require('angular');

  var testService = {};
  testService.module = angular.module('testServiceModule', []);
  testService.module.service('testService', function() {
    var data = "Service Data";

    this.getServiceData = function() {
      return data;
    }
  });
  return testService;
});
