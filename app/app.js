/**
 * Defines the main Angular entry point App. Establishes all the module
 * dependencies and loads the required resources.
 */
define(function(require) {

  var angular = require('angular'),
      navbar = require('nav/navbar'),
      home = require('home/home'),
      test = require('test/test'),
      testService = require('services/test-service');

  require('angularAnimate');
  require('uiRouter');

  var app = {};
  app.module = angular.module('app', [
      // Named Global Modules
      'ngAnimate',
      'ui.router',

      // RequireJS Local Modules
      navbar.module.name,
      home.module.name,
      test.module.name,
      testService.module.name
  ]);

  return app;
});