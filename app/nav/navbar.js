/**
 * Defines the module and directive for rendering the top tab bar.
 */
define(function(require) {
  var angular = require('angular');
  var navbar = {};
  navbar.module = angular.module("navbar", []);
  navbar.module.directive('navbar', function() {
    return {
      restrict: 'E',
      templateUrl: 'app/nav/navbar.html'
    }
  });
  /**
   * @param {number} stickyTabs
   */
  navbar.module.directive('stickyTabs', function($window) {
    var windowElem = angular.element($window);

    function getYOffset() {
      return (angular.isDefined($window.pageYOffset) ?
          $window.pageYOffset :
          $window.document[0].documentElement.scrollTop);
    }

    //noinspection JSUnusedLocalSymbols
    function link(scope, elem, attrs) {
      var positionNormal = elem.css('position'),
          topNormal = elem.css('top'),
          threshold = parseInt(attrs.stickyTabs),
          startFromTop = elem[0].getBoundingClientRect().top + getYOffset(),
          isAffixed;

      function checkPosition() {
        var shouldAffix = (getYOffset() + threshold) > startFromTop;

        if (shouldAffix !== isAffixed) {
          isAffixed = shouldAffix;

          if (shouldAffix) {
            elem.css({
              top: threshold + 'px',
              position: 'fixed'
            })
          } else {
            elem.css({
              top: topNormal,
              position: positionNormal
            })
          }
        }
      }
      windowElem.on('scroll', checkPosition);
    }

    return {
      restrict: 'A',
      scope: {
        stickyTabs: '='
      },
      link: link
    };
  });
  return navbar;
});
