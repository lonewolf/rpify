/**
 * Bootstraps the Angular application. Uses RequireJS to load the base
 * dependencies, then uses Angular to bootstrap the main App module once
 * the DOM is loaded.
 */
define(function(require) {
  require('angular');
  require('app');
  require('routes');

  angular.element(document).ready(function() {
    angular.bootstrap(document, ['app']);
  });
});
