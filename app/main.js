/**
 * Configures RequireJS and bootstraps the application. Called as the main
 * entry point from index.
 *
 * @see bootstrap.js
 */
requirejs.config({
  baseUrl: 'app',
  paths: {
    angular: '../lib/angular.min',
    angularAnimate: '../lib/angular-animate.min',
    uiRouter: '../lib/angular-ui-router.min'
  },
  shim: {
    'angular': {
      exports: 'angular'
    },
    'angularAnimate': {
      deps: ['angular']
    },
    'uiRouter': {
      deps: ['angular']
    }
  },
  // bootstrap the angular app
  deps: ['bootstrap'],

  urlArgs: "bust=" + (+new Date)
});
