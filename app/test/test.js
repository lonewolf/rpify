/**
 * Defines a random test module and controller for a subpage named 'Test'.
 */
define(function(require) {
  var angular = require('angular');
  var test = {};
  test.module = angular.module("test", []);
  test.module.controller('TestCtrl', ['testService', function(testService) {
    this.getInfoFromService = function() {
      return testService.getServiceData();
    }
  }]);
  return test;
});
