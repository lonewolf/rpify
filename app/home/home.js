/**
 * Defines the module and controller for the main ui-view.
 */
define(function(require) {

  var angular = require('angular');

  var home = {};
  home.module = angular.module("home", []);
  home.module.controller('HomeCtrl', function() {
    var data = "hello there";

    this.testMethod = function() {
      return data;
    }
  });
  return home;
});